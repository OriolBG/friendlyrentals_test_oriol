﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace fr_stringcalculator
{
    public class StringCalculator
    {
        private readonly Regex newSeparator_reg = new Regex(@"^//([^-])\n");
        private readonly Regex negative_reg = new Regex(@"-\d+");

        public int Add(string numbers)
        {
            if (String.IsNullOrEmpty(numbers))
                return 0;
            if (negative_reg.IsMatch(numbers))
            {
                var negativeNumbers = new List<string>();
                foreach(Match match in negative_reg.Matches(numbers))
                {
                    negativeNumbers.Add(match.Value);
                }

                throw new NegativeException() { Numbers = negativeNumbers };
            }

            var separators = new List<char>() { '\n', ','};
            var input = numbers;
            if (newSeparator_reg.IsMatch(numbers))
            {
                // the new separator first group is just one character string also we don't allow negative sign 
                var newSeparator = newSeparator_reg.Match(numbers);
                separators.Add(newSeparator.Groups[1].Value[0]);
                input = numbers.Substring(newSeparator.Value.Length);
            }

            var result = input.Split(separators.ToArray())
                .Sum(n => Convert.ToInt32(n));
            return result;
        }
    }
}
