﻿using fr_stringcalculator;
using NUnit.Framework;

namespace fr_stringcalculator_test
{
    [TestFixture]
    public class StringCalculatorTest
    {
        private StringCalculator calculator;

        [OneTimeSetUp]
        public void SetUp()
        {
            this.calculator = new StringCalculator();
        }

        [TestCase("", 0)]
        [TestCase("1", 1)]
        [TestCase("1,2", 3)]
        public void T01_Add_Arguments(string addArguments, int expected)
        {
            var result = this.calculator.Add(addArguments);
            Assert.AreEqual(expected, result);
        }

        [Test]
        public void T02_Add_UnknownNumberOfArguments()
        {
            var result = this.calculator.Add("1,2,5,1,2,0");
            Assert.AreEqual(11, result);
        }

        [Test]
        public void T03_Add_AllowNL()
        {
            var result = this.calculator.Add("1,2\n3");
            Assert.AreEqual(6, result);
        }

        [Test]
        public void T04_Add_CustomDelimiter()
        {
            var result = this.calculator.Add("//;\n1;2");
            Assert.AreEqual(3, result);
        }

        [Test]
        public void T05_Add_CustomDelimiterWithCommasOrNL()
        {
            var result = this.calculator.Add("//;\n1;2\n3,4");
            Assert.AreEqual(10, result);
        }

        [Test]
        public void T06_Add_ExceptionOnNegativeNumbers()
        {
            Assert.Throws(
                Is.TypeOf<NegativeException>(),
                () => this.calculator.Add("0,-1"));

            Assert.Throws(
                Is.TypeOf<NegativeException>()
                .And.Property("Numbers").Contains("-1")
                .And.Property("Numbers").Contains("-2"),
                () => this.calculator.Add("-1,-2"));
        }
    }
}
